package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: 连接类型枚举
 * Author: vapeshop
 * Date: 2022/6/20 14:10:57
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:10:57
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum Link {
    /**
     * 与条件 默认
     */
    AND,
    /**
     * 或条件
     */
    OR;
}
