package com.easy.mongodb.common.params;

import java.io.Serializable;

/**
 * ProjectName: easy-mongodb
 * Description: 函数式接口
 * Author: vapeshop
 * Date: 2022/6/20 14:13:24
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:13:24
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@FunctionalInterface
public interface SFunction<T, R> extends Serializable {
    R apply(T t);
}
