package com.easy.mongodb.common.annotation;

import com.easy.mongodb.common.enums.SortDirectionEnum;

import java.lang.annotation.*;

/**
 * ProjectName: easy-mongodb
 * Description: 主键注解
 * Author: vapeshop
 * Date: 2022/6/20 13:20:13
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 13:20:13
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Inherited
@Documented
@Target({ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
public @interface IndexField {
    String name();

    SortDirectionEnum direction() default SortDirectionEnum.ASC;

}
