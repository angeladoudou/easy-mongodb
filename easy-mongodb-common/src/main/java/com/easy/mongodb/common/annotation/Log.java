package com.easy.mongodb.common.annotation;


import com.easy.mongodb.common.enums.OperateType;

import java.lang.annotation.*;

/**
 * @description: 输出mongodb执行日志
 * @author: gankench@gmail.com
 * @create: 2021-04-07 19:38
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
    /**
     * 方法描述,可使用占位符获取参数:{{tel}}
     */
    String detail() default "";

    /**
     * 操作类型(enum):主要是select,insert,update,delete
     */
    OperateType operateType() default OperateType.UNKNOWN;

}
