package com.easy.mongodb.common.utils;

/**
 * ProjectName: easy-mongodb
 * Description: array工具类
 * Author: vapeshop
 * Date: 2022/6/20 14:13:36
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:13:36
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class ArrayUtils {
    private ArrayUtils() {
    }

    /**
     * 判断数据是否为空
     *
     * @param array 数组
     * @return 布尔
     */
    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    /**
     * 判断数组是否不为空
     *
     * @param array 数组
     * @return 布尔
     */
    public static boolean isNotEmpty(Object[] array) {
        return !isEmpty(array);
    }
}
