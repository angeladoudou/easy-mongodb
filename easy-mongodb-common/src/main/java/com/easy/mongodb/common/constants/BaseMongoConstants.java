package com.easy.mongodb.common.constants;


/**
 * @ProjectName: easy-mongodb
 * @Description: EasyMongoDB的常量
 * @Author: vapeshop
 * @Date: 2022/6/17 16:39:10
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/17 16:39:10
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface BaseMongoConstants {
    /**
     * 是否启用本框架前缀, 默认启用
     */
    String ENABLE_PREFIX = "easy-mongodb.enable";
    /**
     * 默认主键名称
     */
    String DEFAULT_ID_NAME = "id";
    /**
     * mongodb 默认的主键名称
     */
    String DEFAULT_MONGO_ID_NAME = "_id";
    /**
     * 数字0
     */
    Integer ZERO = 0;
    /**
     * 数字1
     */
    Integer ONE = 1;
    /**
     * 默认的当前页码
     */
    Integer PAGE_NUM = 1;
    /**
     * 默认的每页显示条目数
     */
    Integer PAGE_SIZE = 10;
    /**
     * 空字符串
     */
    String EMPTY_STR = "";
    /**
     * 内置mongodb分布式锁索引名称
     */
    String LOCK_COLLECTION = "distribute-lock-doc";
    /**
     * get 方法前缀
     */
    String GET_FUNC_PREFIX = "get";
    /**
     * set 方法前缀
     */
    String SET_FUNC_PREFIX = "set";
    /**
     * 基本数据类型的get方法前缀
     */
    String IS_FUNC_PREFIX = "Is";
    /**
     * 索引特性
     */
    String PROPERTIES = "properties";
    /**
     * 字段类型
     */
    String TYPE = "type";
    /**
     * 日期格式化
     */
    String FORMAT = "format";
    /**
     * 分词器
     */
    String ANALYZER = "analyzer";
    /**
     * 查询分词器
     */
    String SEARCH_ANALYZER = "search_analyzer";
    /**
     * 字段关系
     */
    String RELATIONS = "relations";
    /**
     * 父子类型-父id字段
     */
    String PARENT = "parent";
    /**
     * 通配符
     */
    String WILDCARD_SIGN = "*";
    /**
     * 默认返回数
     */
    Integer DEFAULT_SIZE = 10000;
    /**
     * 嵌套类型 path和field连接符
     */
    String PATH_FIELD_JOIN = ".";
    /**
     * 获取/释放 分布式锁 最大失败重试次数
     */
    Integer LOCK_MAX_RETRY = 5;
}
