package com.easy.mongodb.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ProjectName: easy-mongodb
 * Description: 表注解（集合名）
 * Author: vapeshop
 * Date: 2022/6/28 11:18:15
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/28 11:18:15
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface Table {
    /**
     * 实体对应的集合名
     *
     * @return 默认为空
     */
    String value() default "";

    /**
     * 实体对应的数据库名
     *
     * @return 默认为空
     */
    String database() default "";

    Index[] indexs() default {};

    /**
     * 是否保持使用全局的 tablePrefix 的值
     * 只生效于 既设置了全局的 tablePrefix 也设置了上面 value 的值
     * 如果是 false , 全局的 tablePrefix 不生效
     *
     * @return 默认为false
     */
    boolean keepGlobalPrefix() default false;

}
