package com.easy.mongodb.common.enums;


import lombok.AllArgsConstructor;

/**
 * ProjectName: easy-mongodb
 * Description: 关键字枚举
 * Author: vapeshop
 * Date: 2022/7/12 15:20:50
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:20:50
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@AllArgsConstructor
public enum Keyword {
    AND("$AND"),
    OR("$OR"),
    NOT("$NOT"),
    IN("$IN"),
    NOT_IN("$nin"),
    LIKE("LIKE"),
    NOT_LIKE("NOT LIKE"),
    EQ("="),
    NE("$ne"),
    GT("$gt"),
    GE("$gte"),
    LT("$lt"),
    LE("$lte"),
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL"),
    HAVING("HAVING"),
    EXISTS("$EXISTS"),
    NOT_EXISTS("NOT EXISTS"),
    BETWEEN("BETWEEN"),
    NOT_BETWEEN("NOT BETWEEN");

    private final String keyword;

    public String getSqlSegment() {
        return this.keyword;
    }
}
