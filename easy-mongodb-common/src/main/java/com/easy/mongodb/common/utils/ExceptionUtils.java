package com.easy.mongodb.common.utils;


import cn.hutool.core.util.StrUtil;
import com.easy.mongodb.common.exception.EasyMongoDBException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;

/**
 * @ProjectName: easy-mongodb
 * @Description: 异常辅助工具类
 * @Author: vapeshop
 * @Date: 2022/6/15 21:53:06
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/15 21:53:06
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public final class ExceptionUtils {

    private ExceptionUtils() {
    }

    /**
     * 返回一个新的异常，统一构建，方便统一处理
     *
     * @param msg    消息
     * @param t      异常
     * @param params 参数
     * @return 自定义异常
     */
    public static EasyMongoDBException build(String msg, Throwable t, Object... params) {
        return new EasyMongoDBException(String.format(msg, params), t);
    }

    /**
     * 重载
     *
     * @param msg    消息
     * @param params 参数
     * @return 自定义异常
     */
    public static EasyMongoDBException build(String msg, Object... params) {
        return new EasyMongoDBException(StrUtil.format(msg, params));
    }

    /**
     * 重载
     *
     * @param t 异常
     * @return 自定义异常
     */
    public static EasyMongoDBException build(Throwable t) {
        return new EasyMongoDBException(t);
    }

    /**
     * 异常包装
     *
     * @param wrapped 异常
     * @return 异常
     */
    public static Throwable unwrapThrowable(Throwable wrapped) {
        Throwable unwrapped = wrapped;
        while (true) {
            if (unwrapped instanceof InvocationTargetException) {
                unwrapped = ((InvocationTargetException) unwrapped).getTargetException();
            } else if (unwrapped instanceof UndeclaredThrowableException) {
                unwrapped = ((UndeclaredThrowableException) unwrapped).getUndeclaredThrowable();
            } else {
                return unwrapped;
            }
        }
    }

}
