package com.easy.mongodb.common.enums;

import java.util.Comparator;
import java.util.Locale;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.common.enums
 * @Description:
 * @Author: vapeshop
 * @Date: 2022/6/21 11:28
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/21 11:28
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum SortDirectionEnum {
    ASC {
        @Override
        public String toString() {
            return "asc";
        }

        @Override
        public int reverseMul() {
            return 1;
        }

        @Override
        public <T> Comparator<T> wrap(Comparator<T> delegate) {
            return delegate;
        }
    },
    DESC {
        @Override
        public String toString() {
            return "desc";
        }

        @Override
        public int reverseMul() {
            return -1;
        }

        @Override
        public <T> Comparator<T> wrap(Comparator<T> delegate) {
            return delegate.reversed();
        }
    };

    private SortDirectionEnum() {
    }


    public static SortDirectionEnum fromString(String op) {
        return valueOf(op.toUpperCase(Locale.ROOT));
    }


    public abstract int reverseMul();

    public abstract <T> Comparator<T> wrap(Comparator<T> var1);
}
