package com.easy.mongodb.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ProjectName: easy-mongodb
 * @Description: 索引策略枚举
 * @Author: vapeshop
 * @Date: 2022/6/16 17:02:15
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/16 17:02:15
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@AllArgsConstructor
public enum ProcessIndexStrategyEnum {
    /**
     * 平滑迁移策略,零停机 默认策略
     */
    SMOOTHLY(1),
    /**
     * 非平滑迁移策略 简单粗暴 备选
     */
    NOT_SMOOTHLY(2),
    /**
     * 用户手动调用API处理索引
     */
    MANUAL(3);
    @Getter
    private Integer strategyType;
}
