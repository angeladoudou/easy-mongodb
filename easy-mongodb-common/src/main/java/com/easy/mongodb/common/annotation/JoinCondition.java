package com.easy.mongodb.common.annotation;

import java.lang.annotation.*;

/**
 * 绑定条件查询的注解
 *
 * @author don
 */
@Inherited
@Documented
@Target({ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
public @interface JoinCondition {
    /**
     * 被关联的Entity，不再需要显示的指明，默认取字段上的声明类型
     */
    Class<?> entity() default Void.class;

    /**
     * 被关联的Entity，不再需要显示的指明，默认取字段上的声明类型
     */
    String field() default "";

    /**
     * 关联Entity所需的自身字段
     */
    String selfField() default "";

    /**
     * 被关联Entity的关联字段，默认为关联Entity的id
     */
    String joinField() default "id";

    /**
     * 中间表Entity，需要对应创建其Mapper
     */
    Class<?> midEntity() default Void.class;


    /**
     * 关联Entity所需的自身字段，中间表字段名
     */
    String selfMidField() default "";

    /**
     * 被关联Entity的关联字段，中间表字段名
     */
    String joinMidField() default "";

}
