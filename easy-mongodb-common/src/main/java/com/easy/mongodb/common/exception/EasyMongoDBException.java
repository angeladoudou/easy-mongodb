package com.easy.mongodb.common.exception;

/**
 * @ProjectName: easy-mongodb
 * @Description: 异常类
 * @Author: vapeshop
 * @Date: 2022/6/15 21:41:17
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/15 21:41:17
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class EasyMongoDBException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public EasyMongoDBException(String message) {
        super(message);
    }

    public EasyMongoDBException(Throwable throwable) {
        super(throwable);
    }

    public EasyMongoDBException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
