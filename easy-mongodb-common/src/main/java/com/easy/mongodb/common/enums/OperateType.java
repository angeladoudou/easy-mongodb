package com.easy.mongodb.common.enums;

/**
 * ProjectName: easy-mongodb
 * Description: 操作类型
 * Author: vapeshop
 * Date: 2022/6/22 16:05:39
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/22 16:05:39
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public enum OperateType {
    UNKNOWN("unknown"),
    SAVE("SAVE"),
    QUERY("QUERY"),
    COUNT("COUNT"),
    DELETE("DELETE"),
    UPDATE("update");

    private String value;

    OperateType(String type) {
        this.value = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
