package com.easy.mongodb.common.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * ProjectName: easy-mongodb
 * Description: 泛型工具类
 * Author: vapeshop
 * Date: 2022/6/20 15:05:38
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 15:05:38
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class TypeUtils {
    /**
     * 获取接口上的泛型T
     *
     * @param o     类
     * @param index 下标
     * @return 类的类型
     */
    public static Class<?> getInterfaceT(Class o, int index) {
        Type[] types = o.getGenericInterfaces();
        ParameterizedType parameterizedType = (ParameterizedType) types[index];
        Type type = parameterizedType.getActualTypeArguments()[index];
        return checkType(type, index);

    }

    private static Class<?> checkType(Type type, int index) {
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            Type t = pt.getActualTypeArguments()[index];
            return checkType(t, index);
        } else {
            String className = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType"
                    + ", but <" + type + "> is of type " + className);
        }
    }

}
