package com.easy.mongodb.extension.toolkit;

import cn.hutool.core.lang.Assert;
import com.easy.mongodb.core.biz.TableInfo;
import com.easy.mongodb.core.toolkit.TableInfoHelper;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.core.toolkit
 *
 * @Description: 辅助类
 * @Author: vapeshop
 * @Date: 2022/6/29 19:56
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/29 19:56
 * UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public final class MongoHelper {
    /**
     * 获取TableInfo
     *
     * @param clazz 对象类
     * @return TableInfo 对象表信息
     */
    public static TableInfo table(Class<?> clazz) {
        TableInfo tableInfo = TableInfoHelper.getTableInfo(clazz);
        Assert.notNull(tableInfo, "Error: Cannot execute table Method, ClassGenericType not found.");
        return tableInfo;
    }

    /**
     * 判断数据库操作是否成功
     *
     * @param result 数据库操作返回影响条数
     * @return boolean
     */
    public static boolean retBool(Integer result) {
        return null != result && result >= 1;
    }

    /**
     * 判断数据库操作是否成功
     *
     * @param result 数据库操作返回影响条数
     * @return boolean
     */
    public static boolean retBool(Long result) {
        return null != result && result >= 1;
    }

    /**
     * 返回SelectCount执行结果
     *
     * @param result ignore
     * @return int
     */
    public static long retCount(Long result) {
        return (null == result) ? 0 : result;
    }

//    /**
//     * 从list中取第一条数据返回对应List中泛型的单个结果
//     *
//     * @param list ignore
//     * @param <E>  ignore
//     * @return ignore
//     */
//    public static <E> E getObject(Log log, List<E> list) {
//        return getObject(() -> log, list);
//    }
//
//    /**
//     * @since 3.4.3
//     */
//    public static <E> E getObject(Supplier<Log> supplier, List<E> list) {
//        if (CollectionUtils.isNotEmpty(list)) {
//            int size = list.size();
//            if (size > 1) {
//                Log log = supplier.get();
//                log.warn(String.format("Warn: execute Method There are  %s results.", size));
//            }
//            return list.get(0);
//        }
//        return null;
//    }
}
