package com.easy.mongodb.extension.context;

/**
 * @ProjectName: easy-mongodb
 * @Description: interceptorChain上下文
 * @Author: vapeshop
 * @Date: 2022/6/20 09:18:31
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/20 09:18:31
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class InterceptorChainHolder {
    private final static InterceptorChainHolder INSTANCE = new InterceptorChainHolder();
    /**
     * 拦截器链
     */
    protected InterceptorChain interceptorChain;

    public static InterceptorChainHolder getInstance() {
        return INSTANCE;
    }

    /**
     * 添加拦截器
     *
     * @param interceptor 拦截器
     */
    public void addInterceptor(Interceptor interceptor) {
        interceptorChain.addInterceptor(interceptor);
    }

    public InterceptorChain getInterceptorChain() {
        return interceptorChain;
    }

    public void initInterceptorChain() {
        if (interceptorChain == null) {
            interceptorChain = new InterceptorChain();
        }
    }
}
