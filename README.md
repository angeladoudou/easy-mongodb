# 简介 | Intro
---

Easy-MongoDB是一款参考Easy-ES的项目。让习惯使用Mybatis的方式使用调用。
MongoDB其实是比较功能相当丰富的一个数据库，可以文件存储，时序数据库。
目前项目还没完成文件存储，时序数据库的功能。待后续完善。。。。。
# 其他开源项目 | Other Project
---

- [基于RuoYi-Vue 3.8.3改编的MongoDB版本](https://gitee.com/angeladoudou/RuoYi-Vue-MongoDB)
- 上述项目相当于是本项目的一个应用。

# 期望 | Futures
---

> 欢迎提出更好的意见，帮助完善 Easy-MongoDB

# 版权 | License
---

[MIT](https://gitee.com/angeladoudou/easy-mongodb/blob/master/LICENSE)

# 捐赠 | Donate
---


> 您的支持是鼓励我们前行的动力，无论金额多少都足够表达您这份心意。

> 如果您愿意捐赠本项目,推荐直接在右下方通过Gitee直接捐赠.

# 关注我 | About Me
---
![](./docs/images/Java MongoDB交流群群二维码.png)
群名称：
Java MongoDB交流群

群 号：
1170261501
