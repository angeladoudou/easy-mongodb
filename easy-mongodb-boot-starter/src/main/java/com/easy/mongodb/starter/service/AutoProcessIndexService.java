package com.easy.mongodb.starter.service;

import com.mongodb.client.MongoClient;

/**
 * @ProjectName: easy-mongodb
 * @Description: 自动托管索引接口
 * @Author: vapeshop
 * @Date: 2022/6/17 17:11:25
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/17 17:11:25
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface AutoProcessIndexService {
    /**
     * 获取当前策略类型
     *
     * @return 策略类型
     */
    Integer getStrategyType();

    /**
     * 异步处理索引
     *
     * @param entityClass 实体类
     * @param client      MongoClient
     */
    void processIndexAsync(Class<?> entityClass, MongoClient client);

}
