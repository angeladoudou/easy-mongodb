package com.easy.mongodb.test.crud;

import com.easy.mongodb.core.toolkit.Wraps;
import com.easy.mongodb.test.TestEasyMongoDBApplication;
import com.easy.mongodb.test.entity.*;
import com.easy.mongodb.test.mapper.*;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.logging.Logger;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.test.crud
 *
 * @Description:
 * @Author: vapeshop
 * @Date: 2022/7/6 17:58
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/6 17:58
 * UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestEasyMongoDBApplication.class)
public class TestCRUD {
    public static Logger logger = Logger.getLogger("TestCRUD");
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    @Resource
    private IdentityCardMapper cardMapper;
    @Resource
    private StudentMapper studentMapper;
    @Resource
    private SysUserRoleMapper userRoleMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private SysOperLogMapper sysOperLogMapper;

    @Test
    public void findById() {
//        System.out.println(this.sysUserMapper.selectById("62ce93089115fd6db458500e"));
        System.out.println(this.sysUserMapper.selectList(Wraps.<SysUser>lbQ().eq(SysUser::getId, "62ce93089115fd6db458500e")));
    }

    @Test
    public void findTypeIsObjectId() {
//        System.out.println(this.sysUserMapper.selectById("62ce93089115fd6db458500e"));
        System.out.println(this.sysUserMapper.selectList(Wraps.<SysUser>lbQ().eq(SysUser::getDeptId, "62cfe8c44895a216ab7960cd")));
    }

    @Test
    public void find() {
//        this.userMapper.selectList(Wrappers.lbQ(User.class)).stream().forEach(System.out::println);
        this.sysUserMapper.selectList(Wraps.<SysUser>lbQ().eq(SysUser::getUserName, "ry")).stream().forEach(System.out::println);
    }

    @Test
    public void findo() {
//        this.userMapper.selectList(Wrappers.lbQ(User.class)).stream().forEach(System.out::println);
        this.sysUserMapper.selectList(Wraps.<SysUser>lbQ().eq(SysUser::getUserName, "admin")).stream().forEach(System.out::println);
    }

    @Test
    public void findMid() {
//        this.userMapper.selectList(Wrappers.lbQ(User.class)).stream().forEach(System.out::println);
        this.sysUserRoleMapper.selectList(Wraps.<SysUserRole>lbQ()).stream().forEach(System.out::println);
    }

    @Test
    public void findSysRole() {
//        this.userMapper.selectList(Wraps.lbQ()).stream().forEach(System.out::println);
//        this.userMapper.selectList(Wrappers.lbQ(User.class)).stream().forEach(System.out::println);
        this.sysRoleMapper.selectList(Wraps.<SysRole>lbQ()).stream().forEach(System.out::println);
    }

    @Test
    public void page() {
        SysOperLog user = new SysOperLog();
        System.out.println(this.sysOperLogMapper.page(Wraps.lbQ(user), 1, 10));
    }

    @Test
    public void findEntity() {
        SysUser user = new SysUser();
        user.setUserName("ry");
        this.sysUserMapper.selectList(user).stream().forEach(System.out::println);
    }

    @Test
    public void insertMid() {
        SysUserRole userRole = new SysUserRole();
        userRole.setRoleId("62cfe8c54895a216ab7961ad");
        userRole.setUserId("62cfe8c64895a216ab7961b2");
        this.userRoleMapper.insert(userRole);
        System.out.println(this.userRoleMapper.selectList(Wraps.lbQ(SysUserRole.class).eq(SysUserRole::getRoleId, "62cfe8c54895a216ab7961ad").eq(SysUserRole::getUserId, "62cfe8c64895a216ab7961b2")));
    }

    @Test
    public void insert() {

        SysUser user = new SysUser();
        this.sysUserMapper.insert(user);
        IdentityCard card = new IdentityCard();
        card.setUserId("UserId");
        card.setIdentityNumber("1111111111");
        this.cardMapper.insert(card);
    }

    @Test
    public void insertOther() {
        Date now = new Date();
//        BasicDBObject time = new BasicDBObject("ts", now);
        Document time = new Document("ts", now);
        this.sysUserMapper.getMongoDatabase().getCollection("aaa").insertOne(time);
    }

    @Test
    public void findOr() {
        this.sysUserMapper.selectList(Wraps.<SysUser>lbQ().like(SysUser::getNickName, "若依").or(i -> i.like(SysUser::getPhonenumber, "6666"))).stream().forEach(System.out::println);
    }

    @Test
    public void findBetween() {
        System.out.println(this.studentMapper.selectList(Wraps.lbQ(Student.class).between(Student::getAge, "20", "24")));
        System.out.println(this.sysUserMapper.selectList(Wraps.lbQ(SysUser.class).between(SysUser::getCreateTime, "2022-07-22 05:42:45.162", "2022-07-27 05:42:45.162")));
    }

    @Test
    public void updateById() {
        SysUser user = new SysUser("62da38d55c94db7923b9445c");
        user.setUserName("falsexxxxxxxx");
        user.setAdmin(false);
        this.sysUserMapper.updateById(user);
    }

    @Test
    public void update() {
        SysUser user = new SysUser();
        user.setUserName("falsexxxxxxxx");
        user.setAdmin(false);
//        this.sysUserMapper.update(user);
    }

    @Test
    public void deleteById() {
        SysUser user = new SysUser("62da389d6bc533145e6b7b30");
        this.sysUserMapper.deleteById(user);
    }

    @Test
    public void deleteByWrap() {
        this.sysUserMapper.delete(Wraps.<SysUser>lbQ().eq(SysUser::getUserName, "xxxxxxxx"));
    }

    @Test
    public void sort() {
        System.out.println(this.studentMapper.selectList(Wraps.lbQ(Student.class).between(Student::getAge, "20", "24").orderByAsc(Student::getAge).orderByDesc(Student::getScore)));
    }

    @Test
    public void count() {
        logger.info(this.studentMapper.selectCount(Wraps.<Student>lbQ()).toString());
    }

}
