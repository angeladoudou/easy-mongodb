package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.Role;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.test.mapper
 *
 * @Description:
 * @Author: vapeshop
 * @Date: 2022/7/6 13:37
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/6 13:37
 * UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface RoleMapper extends BaseMongoMapper<Role> {
}
