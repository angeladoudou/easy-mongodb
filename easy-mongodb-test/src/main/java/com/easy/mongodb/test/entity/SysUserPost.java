package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import org.bson.types.ObjectId;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author ruoyi
 */
@Table(value = "sys_user_post")
public class SysUserPost {
    /** 用户ID */
    private ObjectId userId;

    /** 岗位ID */
    private ObjectId postId;

    public ObjectId getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = new ObjectId(userId);
    }

    public ObjectId getPostId() {
        return this.postId;
    }

    public void setPostId(String postId) {
        this.postId = new ObjectId(postId);
    }

    @Override
    public String toString() {
        return "SysUserPost{" +
                "userId=" + this.userId +
                ", postId=" + this.postId +
                '}';
    }
}
