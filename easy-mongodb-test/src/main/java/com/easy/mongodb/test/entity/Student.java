package com.easy.mongodb.test.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 创建人:T-baby
 * 创建日期: 16/8/25
 * 文件描述:
 */
@Data
public class Student {

    /**
     * Student是一个英语培训机构的学生类,因为MongoDB本身没有约束,故使用class来约束
     *
     * @param id Objectid
     * @param name 学生姓名
     * @param age 学生年龄
     * @param sex 学生性别,1为男生,2为女生
     * @param score 几次考试总数
     * @param teacher 教课的老师
     */


    private String id;

    private String name;

    private int age;

    private int sex;

    private int score;

    private String teacher;

    private String teacherName;


    public String getId() {
        return this.id;
    }

    @JSONField(name = "_id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return this.sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTeacher() {
        return this.teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTeacherName() {
        return this.teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

}
