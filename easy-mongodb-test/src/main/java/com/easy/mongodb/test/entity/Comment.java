package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Intercepts;
import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableField;
import com.easy.mongodb.common.enums.FieldType;
import lombok.Data;

/**
 * ProjectName: easy-mongodb
 * Description: 评论 数据模型 Document的子文档,Document是其父文档
 * Author: vapeshop
 * Date: 2022/6/20 17:12:19
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 17:12:19
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@Table
@Intercepts({})
public class Comment {
    /**
     * 评论id
     */
    private String id;
    /**
     * 评论内容
     */
    @TableField(fieldType = FieldType.TEXT)
    private String commentContent;
    /**
     * 父子关系字段
     */
//    @TableField(fieldType = FieldType.JOIN)
//    private JoinField joinField;
}
