package com.easy.mongodb.test.entity;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 创建人:T-baby
 * 创建日期: 16/8/25
 * 文件描述:
 */
public class Teacher {

    /**
     * Teacher是一个英语培训机构的老师类,因为MongoDB本身没有约束,故使用class来约束
     *
     * @param id Objectid
     * @param name 老师姓名
     */


    private String id;

    private String name;

    public String getId() {
        return this.id;
    }

    @JSONField(name = "_id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
