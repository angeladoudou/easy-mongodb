package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysDictData;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 字典表 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:49:23
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:49:23
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysDictDataMapper extends BaseMongoMapper<SysDictData> {
}
