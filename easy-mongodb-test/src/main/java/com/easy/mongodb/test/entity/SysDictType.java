package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;

/**
 * 字典类型表 sys_dict_type
 *
 * @author ruoyi
 */
@Table(value = "sys_dict_type")
public class SysDictType extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 字典主键 */
//    @Excel(name = "字典主键", cellType = ColumnType.NUMERIC)
//    private Long dictId;

    /** 字典名称 */
    private String dictName;

    /** 字典类型 */
    private String dictType;

    /** 状态（0正常 1停用） */
    private String status;


    public String getDictName() {
        return this.dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDictType() {
        return this.dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysDictType{" +
                "dictName='" + this.dictName + '\'' +
                ", dictType='" + this.dictType + '\'' +
                ", status='" + this.status + '\'' +
                '}';
    }
}
