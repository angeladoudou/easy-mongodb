package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysConfig;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 参数配置 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:50:03
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:50:03
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysConfigMapper extends BaseMongoMapper<SysConfig> {
}
