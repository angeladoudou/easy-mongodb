package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysUser;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 用户表 数据层
 * Author: vapeshop
 * Date: 2022/7/12 15:55:11
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:55:11
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysUserMapper extends BaseMongoMapper<SysUser> {
}
