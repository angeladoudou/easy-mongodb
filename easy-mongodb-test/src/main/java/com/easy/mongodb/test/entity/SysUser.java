package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.JoinCondition;
import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableField;
import com.easy.mongodb.common.enums.FieldType;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
@Table(value = "sys_user")
public class SysUser extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /** 部门ID */
    @TableField(fieldType = FieldType.OBJECT_ID)
    private String deptId;

    /** 用户账号 */
    private String userName;

    /** 用户昵称 */
    private String nickName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    private String loginIp;

    /** 最后登录时间 */
    @TableField
    private Date loginDate;

    /** 部门对象 */
    private SysDept dept;

    /** 角色对象 */
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = SysRole.class,
                    midEntity = SysUserRole.class, selfField = "_id", selfMidField = "user_id", joinMidField = "role_id", joinField = "_id"
            ))
    private List<SysRole> roles;

    /** 角色组 */
//    @TableField(
//            fieldType = FieldType.JOIN,
//            condition = @JoinCondition(
//                    entity = SysRole.class, field = "_id",
//                    midEntity = SysUserRole.class, selfField = "_id", selfMidField = "user_id", joinMidField = "role_id", joinField = "_id"
//            ))
    private String[] roleIds;

    /** 岗位组 */
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = SysPost.class,
                    midEntity = SysUserPost.class, selfField = "_id", selfMidField = "user_id", joinMidField = "post_id", joinField = "_id"
            ))
    private List<SysPost> postIds;

    /** 角色ID */
    private String roleId;
    private Boolean admin;

    public SysUser() {

    }

    public SysUser(String userId) {
        this.setId(userId);
    }

    public Boolean getAdmin() {
        return this.admin;
    }

    public boolean isAdmin() {
        return Objects.isNull(this.admin) ? false : this.admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getDeptId() {
        return this.deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return this.phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getLoginIp() {
        return this.loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLoginDate() {
        return this.loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public SysDept getDept() {
        return this.dept;
    }

    public void setDept(SysDept dept) {
        this.dept = dept;
    }

    public List<SysRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public String[] getRoleIds() {
        return this.roleIds;
    }

    public void setRoleIds(String[] roleIds) {
        this.roleIds = roleIds;
    }

    public List<SysPost> getPostIds() {
        return this.postIds;
    }

    public void setPostIds(List<SysPost> postIds) {
        this.postIds = postIds;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id='" + this.getId() + '\'' +
                ", deptId='" + this.deptId + '\'' +
                ", userName='" + this.userName + '\'' +
                ", nickName='" + this.nickName + '\'' +
                ", email='" + this.email + '\'' +
                ", phonenumber='" + this.phonenumber + '\'' +
                ", sex='" + this.sex + '\'' +
                ", avatar='" + this.avatar + '\'' +
                ", password='" + this.password + '\'' +
                ", status='" + this.status + '\'' +
                ", delFlag='" + this.delFlag + '\'' +
                ", loginIp='" + this.loginIp + '\'' +
                ", loginDate=" + this.loginDate +
                ", dept=" + this.dept +
                ", roles=" + this.roles +
                ", roleIds=" + Arrays.toString(this.roleIds) +
                ", postIds=" + this.postIds +
                ", roleId='" + this.roleId + '\'' +
                ", admin=" + this.admin +
                '}';
    }
}
