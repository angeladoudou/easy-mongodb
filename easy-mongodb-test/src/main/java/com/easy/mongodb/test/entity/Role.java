package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableId;
import lombok.Data;

/**
 * @ProductName: easy-mongodb
 * @Package: com.easy.mongodb.test.entity
 * @Description: 角色信息
 * @Author: vapeshop
 * @Date: 2022/6/28 09:53
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/28 09:53
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Data
@Table(value = "sys_role")
public class Role {
    @TableId
    private String id;
    /** 角色ID */

    private Long roleId;

    /** 角色名称 */
    private String roleName;

    /** 角色权限 */
    private String roleKey;

    /** 角色排序 */
    private String roleSort;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限） */
    private String dataScope;

    /** 菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示） */
    private boolean menuCheckStrictly;

    /** 部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ） */
    private boolean deptCheckStrictly;

    /** 角色状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 用户是否存在此角色标识 默认不存在 */
    private boolean flag = false;

    /** 菜单组 */
    private Long[] menuIds;

    /** 部门组（数据权限） */
    private Long[] deptIds;
}
