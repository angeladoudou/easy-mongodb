package com.easy.mongodb.test.mapper;

import com.easy.mongodb.core.conditions.interfaces.BaseMongoMapper;
import com.easy.mongodb.test.entity.SysNotice;

/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 通知公告表 数据层
 * Author: vapeshop
 * Date: 2022/7/12 16:03:21
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 16:03:21
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface SysNoticeMapper extends BaseMongoMapper<SysNotice> {
}
