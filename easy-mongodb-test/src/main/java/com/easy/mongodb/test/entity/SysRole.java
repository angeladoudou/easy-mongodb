package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.JoinCondition;
import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableField;
import com.easy.mongodb.common.enums.FieldType;

import java.util.List;
import java.util.Objects;

/**
 * 角色表 sys_role
 *
 * @author ruoyi
 */
@Table(value = "sys_role")
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /** 角色名称 */
    private String roleName;

    /** 角色权限 */
    private String roleKey;

    /** 角色排序 */
    private String roleSort;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本人数据权限） */
    private String dataScope;

    /** 菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示） */
    private Boolean menuCheckStrictly;

    /** 部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ） */
    private Boolean deptCheckStrictly;

    /** 角色状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 用户是否存在此角色标识 默认不存在 */
    @TableField(exist = false)
    private Boolean flag = false;

    /** 菜单组 */
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = SysMenu.class, field = "_id",
                    midEntity = SysRoleMenu.class, selfField = "_id", selfMidField = "role_id", joinMidField = "menu_id", joinField = "_id"
            ))
    private List<String> menuIds;

    /** 部门组（数据权限） */
    @TableField(
            fieldType = FieldType.JOIN,
            condition = @JoinCondition(
                    entity = SysDept.class, field = "_id",
                    midEntity = SysRoleDept.class, selfField = "_id", selfMidField = "role_id", joinMidField = "dept_id", joinField = "_id"
            ))
    private List<String> deptIds;

    private Boolean admin;

    public SysRole() {

    }

    public SysRole(String roleId) {
        this.setId(roleId);
    }

    public Boolean getAdmin() {
        return this.admin;
    }

    public boolean isAdmin() {
        return Objects.isNull(this.admin) ? false : this.admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleKey() {
        return this.roleKey;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    public String getRoleSort() {
        return this.roleSort;
    }

    public void setRoleSort(String roleSort) {
        this.roleSort = roleSort;
    }

    public String getDataScope() {
        return this.dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public boolean isMenuCheckStrictly() {
        return this.menuCheckStrictly;
    }

    public void setMenuCheckStrictly(boolean menuCheckStrictly) {
        this.menuCheckStrictly = menuCheckStrictly;
    }

    public boolean isDeptCheckStrictly() {
        return this.deptCheckStrictly;
    }

    public void setDeptCheckStrictly(boolean deptCheckStrictly) {
        this.deptCheckStrictly = deptCheckStrictly;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public boolean isFlag() {
        return this.flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<String> getMenuIds() {
        return this.menuIds;
    }

    public void setMenuIds(List<String> menuIds) {
        this.menuIds = menuIds;
    }

    public List<String> getDeptIds() {
        return this.deptIds;
    }

    public void setDeptIds(List<String> deptIds) {
        this.deptIds = deptIds;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "roleName='" + this.roleName + '\'' +
                ", roleKey='" + this.roleKey + '\'' +
                ", roleSort='" + this.roleSort + '\'' +
                ", dataScope='" + this.dataScope + '\'' +
                ", menuCheckStrictly=" + this.menuCheckStrictly +
                ", deptCheckStrictly=" + this.deptCheckStrictly +
                ", status='" + this.status + '\'' +
                ", delFlag='" + this.delFlag + '\'' +
                ", flag=" + this.flag +
                ", menuIds=" + this.menuIds +
                ", deptIds=" + this.deptIds +
                ", admin=" + this.admin +
                '}';
    }
}
