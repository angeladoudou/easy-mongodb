package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;

/**
 * 参数配置表 sys_config
 *
 * @author ruoyi
 */
@Table(value = "sys_config")
public class SysConfig extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 参数名称 */
    private String configName;

    /** 参数键名 */
    private String configKey;

    /** 参数键值 */
    private String configValue;

    /** 系统内置（Y是 N否） */
    private String configType;

//    public Long getConfigId() {
//        return this.configId;
//    }
//
//    public void setConfigId(Long configId) {
//        this.configId = configId;
//    }

    public String getConfigName() {
        return this.configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigKey() {
        return this.configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getConfigValue() {
        return this.configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getConfigType() {
        return this.configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    @Override
    public String toString() {
        return "SysConfig{" +
                "configName='" + this.configName + '\'' +
                ", configKey='" + this.configKey + '\'' +
                ", configValue='" + this.configValue + '\'' +
                ", configType='" + this.configType + '\'' +
                '}';
    }
}
