package com.easy.mongodb.test.entity;

import com.easy.mongodb.common.annotation.Table;
import com.easy.mongodb.common.annotation.TableField;


/**
 * ProjectName: RuoYi-Vue-MongoDB
 * Description: 岗位表 sys_post
 * Author: vapeshop
 * Date: 2022/7/14 13:45:42
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/14 13:45:42
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@Table(value = "sys_post")
public class SysPost extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /** 岗位编码 */
    private String postCode;

    /** 岗位名称 */
    private String postName;

    /** 岗位排序 */
    private String postSort;

    /** 状态（0正常 1停用） */
    private String status;

    /** 用户是否存在此岗位标识 默认不存在 */
    @TableField(exist = false)
    private boolean flag = false;

    public String getPostCode() {
        return this.postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostName() {
        return this.postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostSort() {
        return this.postSort;
    }

    public void setPostSort(String postSort) {
        this.postSort = postSort;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isFlag() {
        return this.flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "SysPost{" +
                "postCode='" + this.postCode + '\'' +
                ", postName='" + this.postName + '\'' +
                ", postSort='" + this.postSort + '\'' +
                ", status='" + this.status + '\'' +
                ", flag=" + this.flag +
                '}';
    }
}
