package com.easy.mongodb.core.conditions.update;

import com.easy.mongodb.common.params.SFunction;
import com.easy.mongodb.core.biz.UpdateParam;
import com.easy.mongodb.core.conditions.AbstractLambdaWrapper;
import com.easy.mongodb.core.toolkit.FieldUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName: easy-mongodb
 * Description: Lambda 更新封装
 * Author: vapeshop
 * Date: 2022/7/12 15:20:23
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/12 15:20:23
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
@SuppressWarnings("serial")
public class LbuWrapper<T> extends AbstractLambdaWrapper<T, LbuWrapper<T>>
        implements Update<LbuWrapper<T>, SFunction<T, ?>> {
    List<UpdateParam> updateParamList;

    /**
     * SQL 更新字段内容，例如：name='1', age=2
     */

    public LbuWrapper() {
        // 如果无参构造函数，请注意实体 NULL 情况 SET 必须有否则 SQL 异常
        this((T) null);
    }

    public LbuWrapper(T entity) {
        super.setEntity(entity);
        super.initNeed();
        this.updateParamList = new ArrayList<>();
    }

    public LbuWrapper(Class<T> entityClass) {
        super.setEntityClass(entityClass);
        super.initNeed();
        this.updateParamList = new ArrayList<>();
    }

    LbuWrapper(T entity, Class<T> entityClass) {
        super.setEntity(entity);
        super.setEntityClass(entityClass);
        this.updateParamList = new ArrayList<>();
    }


    @Override
    public LbuWrapper<T> set(boolean condition, SFunction<T, ?> column, Object val) {
        return set(condition, FieldUtils.getFieldName(column), val);
    }

    @Override
    public LbuWrapper<T> set(boolean condition, String column, Object val) {
        return maybeDo(condition, () -> {
            UpdateParam updateParam = new UpdateParam();
            updateParam.setField(column);
            updateParam.setValue(val);
            this.updateParamList.add(updateParam);
        });
    }


    @Override
    protected LbuWrapper<T> instance() {
        return new LbuWrapper<>(getEntity(), getEntityClass());
    }


}
