package com.easy.mongodb.core.conditions;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.List;

/**
 * ProjectName: easy-mongodb
 * Description: Lambda表达式的祖宗类
 * Author: vapeshop
 * Date: 2022/6/20 14:42:28
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 14:42:28
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public abstract class Wrapper<T> {
    /**
     * 更新操作作用的索引名
     */
    protected String collectionName;

    /**
     * 实体对象（子类实现）
     *
     * @return 泛型 T
     */
    public abstract T getEntity();

    public String getCollectionName() {
        return this.collectionName;
    }

    public int getSkip() {
        return 0;
    }

    public int getLimit() {
        return 10;
    }

    public Bson getSort() {
        return null;
    }

    public Document getDocument() {
        return null;
    }

    public List<Document> getDocuments() {
        return null;
    }

    public List<Bson> getQuery() {
        return null;
    }

    public Bson getProjection() {
        return null;
    }

    public Bson getConditions() {
        return null;
    }

    public String getId() {
        return null;
    }

    public List<Bson> data() {
        return null;
    }

    /**
     * 条件清空
     */
    abstract public void clear();
}
