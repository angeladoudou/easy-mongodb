package com.easy.mongodb.core.conditions.update;

import com.easy.mongodb.core.toolkit.FieldUtils;

import java.io.Serializable;

/**
 * ProjectName: easy-mongodb
 * Description: Update
 * Author: vapeshop
 * Date: 2022/7/6 14:20:27
 * UpdateUser: vapeshop
 * UpdateDate: 2022/7/6 14:20:27
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public interface Update<Children, R> extends Serializable {

    default Children set(R column, Object val) {
        return set(true, column, val);
    }

    default Children set(String column, Object val) {
        return set(true, column, val);
    }

    default Children set(boolean condition, R column, Object val) {
        return set(condition, FieldUtils.getFieldName(column), val);
    }

    /**
     * 设置 更新 SQL 的 SET 片段
     *
     * @param condition 条件
     * @param column    列
     * @param val       值
     * @return 泛型
     */
    Children set(boolean condition, String column, Object val);

}
