package com.easy.mongodb.core.proxy;

import com.easy.mongodb.core.cache.BaseCache;
import com.easy.mongodb.core.conditions.BaseMongoMapperImpl;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @ProjectName: easy-mongodb
 * @Description: 代理类
 * @Author: vapeshop
 * @Date: 2022/6/17 16:45:46
 * @UpdateUser: vapeshop
 * @UpdateDate: 2022/6/17 16:45:46
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class MongoMapperProxy<T> implements InvocationHandler, Serializable {
    private static final long serialVersionUID = 1L;
    private Class<T> mapperInterface;

    public MongoMapperProxy(Class<T> mapperInterface) {
        this.mapperInterface = mapperInterface;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        BaseMongoMapperImpl<?> baseMongoMapperInstance = BaseCache.getBaseMongoMapperInstance(mapperInterface);
        // 这里如果后续需要像MP那样 从xml生成代理的其它方法,则可增强method,此处并不需要
        return method.invoke(baseMongoMapperInstance, args);
    }

}
