package com.easy.mongodb.core.cache;

import com.easy.mongodb.common.constants.BaseMongoConstants;
import com.easy.mongodb.common.utils.CollectionUtils;
import com.easy.mongodb.common.utils.ExceptionUtils;
import com.easy.mongodb.core.biz.TableInfo;
import com.easy.mongodb.core.conditions.BaseMongoMapperImpl;
import com.easy.mongodb.core.toolkit.FieldUtils;
import com.easy.mongodb.core.toolkit.TableInfoHelper;
import com.mongodb.client.MongoClient;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ProjectName: easy-mongodb
 * Description: 基本缓存
 * Author: vapeshop
 * Date: 2022/6/20 15:36:10
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 15:36:10
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class BaseCache {
    /**
     * 用于存放BaseMongoMapper的所有实例
     */
    private static final Map<String, BaseMongoMapperImpl<?>> baseMongoMapperInstanceMap = new ConcurrentHashMap<>();
    /**
     * 用于存放Mongo entity 中的字段的get/is方法
     */
    private static final Map<String, Map<String, Method>> baseMongoEntityMethodMap = new ConcurrentHashMap<>();

    /**
     * 初始化缓存
     *
     * @param mapperInterface mapper接口
     * @param client          mongo客户端
     * @param entityClass     实体类
     */
    public static void initCache(Class<?> mapperInterface, Class<?> entityClass, MongoClient client) {
        // 初始化baseMongoMapper的所有实现类实例
        BaseMongoMapperImpl baseMongoMapper = new BaseMongoMapperImpl();
        baseMongoMapper.init(client, entityClass);
        baseMongoMapperInstanceMap.put(mapperInterface.getName(), baseMongoMapper);

        // 初始化entity中所有字段(注解策略生效)
        Map<String, Method> invokeMethodsMap = initInvokeMethodsMap(entityClass);
        baseMongoEntityMethodMap.putIfAbsent(entityClass.getName(), invokeMethodsMap);

        TableInfo tableInfo = TableInfoHelper.getTableInfo(entityClass);
        // 初始化嵌套类中的所有方法
        Set<Class<?>> allNestedClass = tableInfo.getAllNestedClass();
        if (CollectionUtils.isNotEmpty(allNestedClass)) {
            allNestedClass.forEach(nestedClass -> {
                Map<String, Method> nestedInvokeMethodsMap = initInvokeMethodsMap(nestedClass);
                baseMongoEntityMethodMap.putIfAbsent(nestedClass.getName(), nestedInvokeMethodsMap);
            });
        }

        // 初始化父子类型JoinField中的所有方法
//        Map<String, Method> joinInvokeMethodsMap = initInvokeMethodsMap(entityInfo.getJoinFieldClass());
//        BaseCache.baseMongoEntityMethodMap.putIfAbsent(entityInfo.getJoinFieldClass(), joinInvokeMethodsMap);
    }


    /**
     * 初始化get及set方法容器
     *
     * @param clazz 类
     * @return 指定类的get及set方法容器
     */
    private static Map<String, Method> initInvokeMethodsMap(Class<?> clazz) {
        Method[] methods = clazz.getMethods();
        Map<String, Method> invokeMethodsMap = new ConcurrentHashMap<>(methods.length);
        Arrays.stream(methods)
                .forEach(entityMethod -> {
                    String methodName = entityMethod.getName();
                    if (methodName.startsWith(BaseMongoConstants.GET_FUNC_PREFIX) || methodName.startsWith(BaseMongoConstants.IS_FUNC_PREFIX)
                            || methodName.startsWith(BaseMongoConstants.SET_FUNC_PREFIX)) {
                        invokeMethodsMap.put(methodName, entityMethod);
                    }
                });
        return invokeMethodsMap;
    }

    /**
     * 获取缓存中对应的BaseMongoMapperImpl
     *
     * @param mapperInterface mapper接口
     * @return 实现类
     */
    public static BaseMongoMapperImpl<?> getBaseMongoMapperInstance(Class<?> mapperInterface) {
        return Optional.ofNullable(baseMongoMapperInstanceMap.get(mapperInterface.getName()))
                .orElseThrow(() -> ExceptionUtils.build("{}no such instance:{}", baseMongoMapperInstanceMap, mapperInterface));
    }

    /**
     * 获取缓存中对应entity和methodName的getter方法
     *
     * @param entityClass 实体
     * @param methodName  方法名
     * @return 执行方法
     */
    public static Method getterMethod(Class<?> entityClass, String methodName) {
        return Optional.ofNullable(baseMongoEntityMethodMap.get(entityClass.getName()))
                .map(b -> b.get(BaseMongoConstants.GET_FUNC_PREFIX + FieldUtils.firstToUpperCase(methodName)))
                .orElseThrow(() -> ExceptionUtils.build("{}:no such method:{}", entityClass, methodName));
    }

    /**
     * 获取缓存中对应entity和methodName的setter方法
     *
     * @param entityClass 实体
     * @param methodName  方法名
     * @return 执行方法
     */
    public static Method setterMethod(Class<?> entityClass, String methodName) {
        return Optional.ofNullable(baseMongoEntityMethodMap.get(entityClass.getName()))
                .map(b -> b.get(BaseMongoConstants.SET_FUNC_PREFIX + FieldUtils.firstToUpperCase(methodName)))
                .orElseThrow(() -> ExceptionUtils.build("{}:no such method:{}", entityClass, methodName));
    }
}
