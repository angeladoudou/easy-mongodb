package com.easy.mongodb.core.biz;

import java.util.Locale;

/**
 * ProductName: easy-mongodb
 * Package: com.easy.mongodb.core.biz
 * Description:
 * "$near" ：查询距离某个点最近的坐标点
 * <p>
 * "$geoWithin" ：查询某个形状内的点
 * <p>
 * "$maxDistance" ：最大距离
 * Author: 86631561@163.coom
 * Date: 2022/7/26 17:02
 * UpdateUser: 86631561@163.coom
 * UpdateDate: 2022/7/26 17:02
 * UpdateRemark: The modified content
 * Version: 1.0.0
 * <p>
 * Copyright © 2022 86631561@163.coom Technologies Inc. All Rights Reserved
 **/
public enum ShapeRelation {
    /**
     * Intersection
     * 交叉
     * MongoDB 可以查询与指定几何相交的位置。
     * 这些查询仅适用于球面上的数据。这些查询使用 $geoIntersects 操作符。
     * 仅仅 2dsphere 索引支持交叉查询。
     */
    INTERSECTION("Intersection"),
    /**
     * Proximity
     * 临近
     * MongoDB 可以查询离另一个点最近的点。
     * 接近度查询使用 $near 运算符。
     * $near 运算符需要一个 2d 或 2dsphere 的索引。
     */
    PROXIMITY("Proximity"),
    /**
     * Inclusion
     * 包含
     * MongoDB 对完全包含在一个指定的多边形内的位置进行查询。
     * 包含查询使用 $geoWithin 运算符。
     * 2d 和 2dsphere 索引两种都支持包含查询。
     * 对于包含查询，MongoDB 并不需要使用到索引;然而，这种索引将提高查询性能。
     */
    INCLUSION("Inclusion");
    private final String relationName;

    private ShapeRelation(String relationName) {
        this.relationName = relationName;
    }


    public static ShapeRelation getRelationByName(String name) {
        name = name.toLowerCase(Locale.ENGLISH);
        ShapeRelation[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            ShapeRelation relation = var1[var3];
            if (relation.relationName.equals(name)) {
                return relation;
            }
        }

        return null;
    }

    public String getRelationName() {
        return this.relationName;
    }
}
