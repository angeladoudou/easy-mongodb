package com.easy.mongodb.core.cache;

import com.easy.mongodb.core.config.GlobalConfig;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ProjectName: easy-mongodb
 * Description: 全局配置缓存
 * Author: vapeshop
 * Date: 2022/6/20 15:36:23
 * UpdateUser: vapeshop
 * UpdateDate: 2022/6/20 15:36:23
 * UpdateRemark: The modified content
 * Version: 1.0
 * <p>
 * Copyright © 2022 vapeshop Technologies Inc. All Rights Reserved
 **/
public class GlobalConfigCache {

    private static final Map<Class<?>, GlobalConfig> globalConfigMap = new ConcurrentHashMap<>(1);

    public static GlobalConfig getGlobalConfig() {
        return Optional.ofNullable(globalConfigMap.get(GlobalConfig.class))
                .orElseGet(() -> {
                    GlobalConfig globalConfig = new GlobalConfig();
                    GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
                    globalConfig.setDbConfig(dbConfig);
                    return globalConfig;
                });
    }

    public static void setGlobalConfig(GlobalConfig globalConfig) {
        globalConfigMap.putIfAbsent(GlobalConfig.class, globalConfig);
    }

}
